terraform {
  required_providers {
    aws = {
        source = "hashicorp/aws"
    }
  }
}

data "aws_ami" "server_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["801119661308"] # Microsoft
}

resource "aws_vpc" "test-env" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

resource "aws_security_group" "ingress-all-test" {
  name = "allow-all-sg"
  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }
  // Terraform removes the default rule
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
 }
}

provider "aws" {
    profile = "default"
    region = "eu-west-1"
}

# resource "aws_key_pair" "terraform_key_pair" {
#   count = length(var.pems_v2)
#   key_name = var.pems_v2[count.index]
#   public_key = file("keys/terraform-key-pair.pub")
# }

resource "tls_private_key" "pk" {
  count = var.no_of_boxes
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  count = var.no_of_boxes
  key_name = "${var.pem_name}${count.index}"
  public_key = tls_private_key.pk[count.index].public_key_openssh
}

resource "local_file" "ssh_key" {
  count = var.no_of_boxes
  filename = "${aws_key_pair.kp[count.index].key_name}.pem"
  content = tls_private_key.pk[count.index].private_key_pem
}

resource "aws_instance" "devbox" {
  count = var.no_of_boxes
  # ami = var.ec2_resource[var.ami_index].ami
  ami = data.aws_ami.server_ami.id
  instance_type = var.ec2_resource[var.ami_index].ami_type
  tags = {
    Name = "${var.pem_name}${count.index}"
  }
  key_name = "${aws_key_pair.kp[count.index].key_name}"
}

# this will only create this local variable if the filename matches the string value
locals {
  pem_file = [for key in local_file.ssh_key: key if key.filename == "db_grad_box_0.pem_"]
}

# Here I'm showing two different ways of iterating
output "pem_file_name_from_local" {
  value = [for file in local.pem_file: file.filename ]
}
output "pem_file_name" {
  #value = local_file.ssh_key[*].filename
  value = [for file in local_file.ssh_key: file.filename ]
}
output "dev_box_pulic_ip" {
  value = aws_instance.devbox[*].public_ip
}