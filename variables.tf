variable "ec2_resource" {
  type = list(object({
    ami = string
    ami_type = string
  }))
  default = [
    {
        ami = "ami-05e786af422f8082a" // ubuntu box
        ami_type = "t2.medium"
    },
    {
        ami = "ami-0a3a2a49bf7baac68" //  Windows Server 2019 with Desktop
        ami_type = "t2.medium"
    }
  ]
}

# Override this value with - terraform apply -var no_of_boxes=1
variable "no_of_boxes" {
    default = 1
}

variable "pem_name" {
    default = "db_grad_box_" 
}

# Override this value with - terraform apply -var ami_index=1
variable "ami_index" {
    default = 0
}